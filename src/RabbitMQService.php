<?php

namespace Muhfarkhans\RabbitMqService;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQService {

    private $connection;

    public function __construct() {
        $this->_connect();
    }

    private function _connect() {
        $this->connection = new AMQPStreamConnection(
            $_ENV['RABBITMQ_HOST'],
            $_ENV['RABBITMQ_PORT'], 
            $_ENV['RABBITMQ_USERNAME'], 
            $_ENV['RABBITMQ_PASSWORD'],
            $_ENV['RABBITMQ_VHOST']
        );
    }

    public function send(string $queue_name, string | array | object $message) {
        $connectMQ = $this->connection;
        $channelMQ = $connectMQ->channel(); 

        $message_json = json_encode( $message );
        $channelMQ->queue_declare($queue_name, true, false, false, false);

        $msg = new AMQPMessage($message_json);

        $channelMQ->basic_publish($msg, '', $queue_name);
        $channelMQ->close();

        $connectMQ->close();
        return true;
    }

    public function consume(string $queue_name)
    {
        $connectMQ = $this->connection;
        $channelMQ = $connectMQ->channel(); 

        $callback = function ($msg) {
            echo ' [x] Received ', $msg->body, "\n";
        };
        $channelMQ->queue_declare($queue_name, false, false, false, false);
        $channelMQ->basic_consume($queue_name, '', false, true, false, false, $callback);
        echo 'Waiting for new message on test_queue', " \n";
        while ($channelMQ->is_consuming()) {
            $channelMQ->wait();
        }
        $channelMQ->close();
        $connectMQ->close();
    }
}